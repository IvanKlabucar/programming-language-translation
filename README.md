# Programming Language Translation

The final result of these laboratory exercises is a fully functional C compiler written in python.
It translates C code into bytecode for the FRISC processor.
